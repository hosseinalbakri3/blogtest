package main

import (
	"gitlab.com/hosseinalbakri3/blogtest/Controller"
	mux "github.com/gorilla/mux"
	"gitlab.com/hosseinalbakri3/blogtest/Model"

	"log"
	"net/http"

)

func main() {
	controller, err := Controller.NewController("root:hsin0272@tcp/goblog?parseTime=true")
	send
	if err != nil {
		log.Fatal(err)
	}
	router := mux.NewRouter()
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./Static"))))
	router.HandleFunc("/", controller.GetIndexPage)
	router.Methods("GET").Path("/post/{postId}").HandlerFunc(controller.GetSinglePage)
	router.Methods("GET").Path("/cat/{catId}").HandlerFunc(controller.GetIndexPageByCategory)
	router.Methods("GET").Path("/groupby").HandlerFunc(controller.GroupBy)


	router.Methods("POST").Path("/signIn").HandlerFunc(controller.SignIn)
	router.Methods("GET").Path("/login").HandlerFunc(controller.LoginGet)
	router.Methods("POST").Path("/login").HandlerFunc(controller.LoginPost)
	router.Methods("POST").Path("/admin/upload").HandlerFunc(controller.CkUpload)
	adminRouter := router.PathPrefix("/admin").Subrouter()
	adminRouter.Use(controller.AdminAuthMiddleware)
	adminRouter.Methods("GET").Path("/welcome").HandlerFunc(controller.CheckIsAuth)

	adminRouter.Methods("GET").Path("/addcategory").HandlerFunc(controller.CreateGroupGet)
	adminRouter.Methods("POST").Path("/addcategory").HandlerFunc(controller.CreateGroupPost)
	adminRouter.Methods("GET").Path("/categories").HandlerFunc(controller.CategoryList)
	adminRouter.Methods("GET").Path("/deletecategory/{catId}").HandlerFunc(controller.DeleteCategory)
	adminRouter.Methods("GET").Path("/editcategory/{catId}").HandlerFunc(controller.EditCategoryGet)
	adminRouter.Methods("POST").Path("/editcategory/{catId}").HandlerFunc(controller.EditCategoryPost)

	adminRouter.Methods("GET").Path("/articles").HandlerFunc(controller.ArticleList)
	adminRouter.Methods("GET").Path("/createarticle").HandlerFunc(controller.CreateArticleGet)
	adminRouter.Methods("POST").Path("/createarticle").HandlerFunc(controller.CreateArticlePost)
	adminRouter.Methods("GET").Path("/deletearticle/{artID}").HandlerFunc(controller.DeleteArticle)
	adminRouter.Methods("GET").Path("/editarticle/{artID}").HandlerFunc(controller.EditArticleGet)
	adminRouter.Methods("POST").Path("/editarticle/{artID}").HandlerFunc(controller.EditArticlePost)

	http.ListenAndServe(":8080", router)
}


// hero -source="$GOPATH/src/app/template" -dest="./" -extensions=".html,.htm" -pkgname="t" -watch

func aaaaa(sender Model.Sender)  {

}



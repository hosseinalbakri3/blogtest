package util

import (
	ptime "github.com/yaa110/go-persian-calendar"
	"time"
)

func ToPersianDate(t time.Time) string {
	return ptime.New(t).Format("yyyy/MM/dd")
}


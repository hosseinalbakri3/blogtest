package util

import (
	"crypto/sha256"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-ozzo/ozzo-validation"
	"io"
	"io/ioutil"
	"mime/multipart"
	"os"
	"path/filepath"
)

const JwtKey = "thH123isisFjFFwtkPeFyM"

type Claim struct {
	UserName string
	jwt.StandardClaims
}

func ValidatePassword(pass string) error {
	err := validation.Validate(pass,
		validation.NotNil.Error("رمز عبور نمیتواند خالی باشد"),
		validation.Required.Error("رمز عبور نمیتواند خالی باشد"),
		validation.Length(5, 50).Error("رمز عبود باید بین 5 تا 50 حرف یا عدد باشد"), )
	return err
}

func GenerateFileName(file multipart.File , name string) (string , error){
	sha := sha256.New()

	_, err := io.Copy(sha, file)
	if err !=nil {
		return "", nil
	}
	_, _ = file.Seek(0, 0)

	return fmt.Sprintf("%X",sha.Sum(nil))+name , nil
}

func UploadFile(file multipart.File , path , name string) error {

	fBites, err := ioutil.ReadAll(file)
	if err != nil{
		return err
	}
	mainDir, err := os.Getwd()
	if err != nil{
		return err
	}

	picPath := filepath.Join(mainDir , path , name)
	pic, err := os.Create(picPath)
	defer pic.Close()
	if err != nil{
		return err
	}

	n, err := pic.Write(fBites)
	if err != nil{
		return err
	}

	fmt.Println(n)

	return nil
}

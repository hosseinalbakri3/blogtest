package Model

import (
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation"
	"time"
)

const articleTable = "articles"

const titleErr = "سربرگ مقاله نباید خالی باشد"
const LongDesc = "توضیحات باید کامل باشد"
const imageErr = "عکس نمیتواند خالی باشد"

type Article struct {
	ID        int      `gorm:"primary_key"`
	Title     string    `gorm:"type:varchar(100);not null"`
	ShortDesc string    `gorm:"type:varchar(1000);not null"`
	LongDesc  string    `gorm:"type:text;not null"`
	Image     string    `gorm:"type:varchar(256);not null"`
	CatId     int       `gorm:not null`
	CreatedAt time.Time `json:"createdAt" gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `json:"updatedAt" gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

func (article Article) Validation() (error, []string) {
	err := validation.Errors{
		"title":     validation.Validate(article.Title, validation.Required.Error(titleErr)),
		"LongDesc":  validation.Validate(article.LongDesc, validation.Required.Error(LongDesc)),
		"ShortDesc": validation.Validate(article.ShortDesc, validation.Required.Error(LongDesc)),
		"image":     validation.Validate(article.Image, validation.Required.Error(imageErr)),
	}

	var errText []string
	for _, value := range err {
		if value != nil {
			errText = append(errText, value.Error())
		}
	}

	return err.Filter(), errText
}

func (handler MHandler) CreateArticle(article *Article) error {

	if result := handler.db.Table(articleTable).Create(article); result.Error != nil {
		return result.Error
	}

	return nil
}

func (handler MHandler) GetALLArticle() ([]Article, error) {
	var articles []Article
	if result := handler.db.Table("articles").Find(&articles); result.Error != nil {
		return articles, result.Error
	}
	return articles, nil
}

func (handler MHandler) UpdateArticle(article Article) error {
	if result := handler.db.Table(articleTable).Where("id = ?", article.ID).Update(&article); result.Error != nil {
		return result.Error
	}
	return nil
}

func (handler MHandler) GetArticleById(id int) (Article, error) {
	var article Article
	if result := handler.db.Table("articles").Find(&article, id); result.Error != nil {
		return article, result.Error
	}

	return article, nil
}

func (handler MHandler) GetArticleByCategory(catId int) ([]Article, error) {
	var articles []Article

	if result := handler.db.Table("articles").
		Where("cat_id = ?", catId).Find(&articles); result.Error != nil {
		return articles, result.Error
	}

	return articles, nil
}

func (handler MHandler) DeleteArticle(id int) error {
	if result := handler.db.Table(articleTable).Where(" id = ?", id).Delete(&Article{}); result.Error != nil {
		return result.Error
	}
	return nil
}

func (handler MHandler) GroupBy() {
	type Result struct {
		Year  string
		Month string
		Count int32
	}
	var results []Result
	handler.db.Table("articles").
		Select("EXTRACT(YEAR_MONTH FROM created_at) as ch, YEAR(created_at) as year, MONTHNAME(created_at) as month,Count(*) as count").
		Group("ch").Scan(&results)

	fmt.Println(results)
}

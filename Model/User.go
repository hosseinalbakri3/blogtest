package Model

import (
	"time"
)

type User struct {
	ID           uint      `gorm:"primary_key"`
	UserName     string    `gorm:"type:varchar(256);not null"`
	HashPassword string    `gorm:not null`
	CreatedAt    time.Time `json:"createdAt" gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt    time.Time `json:"updatedAt" gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

func (handler MHandler) CreateUser(user User) (*User, error) {
	var nUser *User
	if result := handler.db.Create(&user); result.Error != nil {
		return nil, result.Error
	} else {
		nUser = result.Value.(*User)
	}
	return nUser, nil
}

func (handler MHandler) GetUserByUsername(username string) (*User , error){
	var user User
	if result := handler.db.Table("users").Where(" user_name = ? ", username ).
		First(&user); result.Error != nil{
		return nil , result.Error
	}
	return &user , nil
}

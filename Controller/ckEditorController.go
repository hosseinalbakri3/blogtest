package Controller

import (
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

//CkUpload handles /ckupload route
func (c Controller) CkUpload(w http.ResponseWriter, r *http.Request) {

	_ = r.ParseForm()
	err := r.ParseMultipartForm(32 << 20)
	if err != nil {
		log.Printf("ERROR: %s\n", err)
		http.Error(w, err.Error(), 500)
		return
	}
	mpartFile, mpartHeader, err := r.FormFile("upload")
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer mpartFile.Close()
	uri, err := saveFile(mpartHeader, mpartFile)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	ckF:= r.Form["CKEditorFuncNum"]
	res:= "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction(\""+ckF[0]+"\", \""+uri+"\" ,'click on ok to continue');</script>"
	fmt.Fprintln(w , res)
}

//saveFile saves file to disc and returns its relative uri
func saveFile(fh *multipart.FileHeader, f multipart.File) (string, error) {
	fileExt := filepath.Ext(fh.Filename)
	newName := fmt.Sprint(time.Now().Unix()) + fileExt //unique file name based on timestamp. You can keep original name and ignore duplicates
	uri := "/static/img/" + newName
	fullName := filepath.Join("static/img", newName)

	file, err := os.OpenFile(fullName, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return "", err
	}
	defer file.Close()
	_, err = io.Copy(file, f)
	if err != nil {
		return "", err
	}
	return uri, nil
}

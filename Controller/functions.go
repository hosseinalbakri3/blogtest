package Controller

import (
	"errors"
	"net/http"
	"time"

	"gitlab.com/hosseinalbakri3/blogtest/Util"
)

func GenerateNewToken(username string, rTime time.Time) (string, error) {

	jwtClim := util.Claim{
		UserName:       username,
		StandardClaims: jwt.StandardClaims{ExpiresAt: rTime.Unix()},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &jwtClim)
	tokenString, err := token.SignedString([]byte(JwtKey))
	if err != nil {
		return tokenString, err
	}

	return tokenString, err
}

func IsLogIn(c Controller, r *http.Request) error {
	co, err := r.Cookie("JwtToken")
	if err != nil {
		return errors.New("not logged in")
	}

	tokenString := co.Value

	token, err := jwt.ParseWithClaims(tokenString, &util.Claim{}, func(token *jwt.Token) (i interface{}, e error) {
		return []byte(JwtKey), nil
	})
	if !token.Valid {
		return errors.New("not logged in")
	}
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return errors.New("خطا در توکن ارسال شده")
		}
		return errors.New("خطا در توکن ارسال شده")
	}

	ok := c.handler.TokenIsExist(tokenString)
	if !ok {
		return errors.New("خطا در توکن ارسال شده")
	}
	return nil
}
